#! /usr/bin/env python
import os
import sys
import subprocess
import yaml

configFile = os.path.join(
    os.getenv('HOME',''),
    '.config', 'archkeys.yaml'
)

def parseConfigFile():
    if not os.path.exists(configFile):
        print(f'No config file found, creating base config at: {configFile}')
        config = {'keyrings': [{'archlinux-keyring':'archlinux'}]}
        with open(configFile, 'w') as cf:
            cf.write(yaml.dump(config))
    else:
        print(f'loading {configFile}')
        with open(configFile) as cf:
            config = yaml.full_load(cf.read())

    keyrings = []
    keys =[]
    for keyring in config['keyrings']:
        for k,v in keyring.items():
            keyrings.append(k)
            keys.append(v)
    return keyrings, keys

def init_cmds(keyrings, keys):
    return [
    f'sudo pacman -S {" ".join(keyrings)}',
    'sudo pacman-key --init',
    f'sudo pacman-key --populate {" ".join(keys)}',
    'sudo pacman-key --refresh-keys',
    'sudo pacman -Syy'
]

def cmds(keyrings):
    return [
    f'sudo pacman -S {" ".join(keyrings)}',
    'sudo pacman -Syy'
]

def main():
    if ('-h' in sys.argv) or ('--help' in sys.argv):
        print('Usage:\nupdatearchkeys  updates arch keys\nupdatearchkeys -i   undate and initiilize keys' )
    else:
        keyrings, keys = parseConfigFile()
        if ('-i' in sys.argv) or ('--init' in sys.argv):
            print('Updating and initilizing keys')
            for cmd in init_cmds(keyrings, keys):
                subprocess.run(cmd, shell=True)
        else:
            print(f'Updating pacman keys from {configFile}')
            for cmd in cmds(keyrings):
                subprocess.run(cmd, shell=True)

if __name__ == '__main__':
    main()