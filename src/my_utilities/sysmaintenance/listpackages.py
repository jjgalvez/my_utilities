#!/usr/bin/env python3

import subprocess
import re
import click

groupsToExclude = [
    'cybergalvez',
    'base',
    'base-devel',
    'gnome',
    'gnome-extra'
]

nativePkgCmd = 'sudo pacman -Qent'
foreignPkgCmd = 'sudo pacman -Qm'
groupsToExcludeCmd = f'sudo pacman -Sg {" ".join(groupsToExclude)}'

def getPname(x):
    y = x.split(' ')
    if y[0] in groupsToExclude:
        return y[1]
    else:
        return y[0]


def getPkgList(cmd):
    return [
        getPname(x) for x in \
            subprocess.run(cmd,
            shell=True, encoding='utf=8',
            capture_output=True).stdout.split('\n') \
            if x
    ]



def printPkgList(cmd):
    pkgsToExclude =  set(getPkgList(groupsToExcludeCmd))
    for p in set(getPkgList(cmd))-pkgsToExclude:
        print(p)


@click.group()
def cli():
    pass


@cli.command()
def native():
    '''
    list all nativelyu installed packages except for thoses in standard goups such as base and gnome
    '''
    printPkgList(foreignPkgCmd)

@cli.command()
def aur():
    '''
    list foreign / aur packages
    '''
    printPkgList(foreignPkgCmd)

if __name__ == '__main__':
    cli()