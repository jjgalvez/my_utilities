#!/usr/bin/env python

import datetime
import sys
import pathlib
import subprocess

def setup():
    today = datetime.date.today()
    goback = input('Enter number of days to go back in time: ').strip()

    if ( goback.isnumeric()):
        goback = (today - datetime.timedelta(days=int(goback))).strftime('%Y/%m/%d')
    else:
        sys.exit()
    return today, goback

def main(today, goback):
    if input(f'Go back in time to {goback}? [y/n]: ').strip().lower() != 'y':
        print('No time travel today')
        input('Press any key to continue')
        sys.exit()
    
    print(f'Going back in time to {goback}')

    mirrorlist = pathlib.Path('/etc/pacman.d/mirrorlist')
    mirrorlistnew = mirrorlist.with_name(f'mirrorlist.{today}')
    mirrorlist.rename(mirrorlistnew)
    print(f'Mirrorlist saved as {mirrorlistnew}')
    with open(mirrorlist, 'w') as m:
        print('Modifying mirrorlist')
        m.write(f'Server=https://archive.archlinux.org/repos/{goback}/$repo/os/$arch\n')
    
    subprocess.run('sudo Pacman -Syyuu', shell=True)


if __name__ == '__main__':
    today, goback = setup()
    main(today, goback)
    input(f'System reverted back to {goback} you should reboot now')
