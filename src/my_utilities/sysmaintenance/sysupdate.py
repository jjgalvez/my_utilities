#!/usr/bin/env python3

import subprocess
import os
import sys
from updatearchkeys import main as updatekeys

class Ask():
    def __init__(self, question: str | None = None,
                 # answer: str | None = None,
                 default: str | None = None,
                 callback: callable = None) -> None:
        self.question = question
        self.default = default
        self.callback = callback

    def setQuestion(self, question) -> None:
        self.question = question

    def setCallback(self, callback) -> None:
        self.callback = callback

    def setDefault(self, default) -> None:
        self.default = default

    def ask(self):
        if a := input(self.question).strip().lower():
            self.callback(a)
        elif self.default:
            self.callback(self.default)

    def __call__(self):
        self.ask()


    
def update_dev_now() -> None:
    print('Updating all -git -svn etc packages now')
    # subprocess.run('pikaur -Sua --devel  --needed', shell=True)
    subprocess.run('paru -Sua --devel  --needed', shell=True)


def help() -> None:
    print('''
    Usage
    updateAll.py  - Updates all systems and aur packages
    Options:
    -u --updatekeys - this will update the arch keyring
    -i --init       - this will update and initiilize the arch keyring
    -f --force    - this will force an update of all -git -svn based
                    aur packages. Normally only devel packges older
                    30 days are checked
    ''')
    input('Press anykey to continue')

def pacdiff() -> None:
    'run pakdiff to look for config chagnes that need attention'
    # subprocess.run('sudo -EH DIFFPROG="meld" pacdiff', shell=True)
    # subprocess.run('DIFFPROG="meld" pacdiff --sudo', shell=True)
    subprocess.run('DIFFPROG="mcdiff" pacdiff --sudo', shell=True)

def check_flatpak() -> None:
    'Since flatpak does not update by itself update it at part of mysystem updates'
    print('updating Flatpak applications')
    subprocess.run('sudo flatpak update -y', shell=True)

def remove_flatpaks() -> None:
    'clean up flatpaks'
    print('Removing unused flatpaks')
    subprocess.run('sudo flatpak uninstall --unused', shell=True)

def reboot_now() -> None:
    'schedule a reboot'
    DE = os.getenv('XDG_CURRENT_DESKTOP', False)
    match DE:
        case 'KDE':
            subprocess.run(['qdbus', 'org.kde.Shutdown', '/Shutdown', 'org.kde.Shutdown.logoutAndReboot'])
        case _:
            subprocess.run('sudo reboot now', shell=True)

def reboot_midnight():
    'schedule a reboot at midnight'
    subprocess.run('sudo shutdown -r 23:30', shell=True)

def logout_DE() -> None:
    'logout of KDE5'
    DE = os.getenv('XDG_CURRENT_DESKTOP', False)

    match DE:
        case 'KDE':
            subprocess.run(['qdbus', 'org.kde.Shutdown', '/Shutdown', 'org.kde.Shutdown.logout'])
        case 'GNOME':
            subprocess.run(['gnome-session-quit', '--no-prompt'])
        case 'XFCE':
            subprocess.run(['xfce4-session-logout', '--logout'])
        case 'LXQT':
            subprocess.run(['lxqt-leave', '--logout'])
        case _:
            print('Sorry you are not running a known DE, logout manually')

def reboot_logout(a):
    match a:
        case 'y': # reboot now
            reboot_now()
        case 'm': # reboot at midnight
            reboot_midnight()
        case 'l': # logog out now
            logout_DE()
        case _:
            ...


def main() -> None:
    # run paru to update system packages and AUR packages
    print('Updating system packages')
    subprocess.run('paru -Syu --combinedupgrade --upgrademenu --skipreview', shell=True)

    Ask(
        question = 'Run Pacdiff to check for config updates? (Y/n): ',
        default = 'y',
        callback = lambda x: pacdiff() if x=='y' else False
    )()

    Ask(
        question = 'Undate Flatpak apps? (Y/n): ',
        default = 'y',
        callback = lambda x: check_flatpak() if x=='y' else False
    )()

    Ask(
        question = 'Remove unused flatpaks? (y/N): ',
        default = 'n',
        callback = lambda x: remove_flatpaks() if x=='y' else False
    )()

    Ask(
        question = 'Reboot / Logout system now or schedule a midnight reboot? (y/N/(m)idnight), (l)ogout: ',
        default = 'n',
        callback = reboot_logout
    )()
    

if __name__ == '__main__':
    args = [x.lower() for x in sys.argv]

    match args:
        case args if '-t' in args or '--timetravel' in args:
            subprocess.run('sudo revertpackages', shell=True)
            sys.exit()
        case args if '-f' in args or '--force' in args:
            update_dev_now()
            main()
        case args if '-h' in args or '--help' in args:
            help()
            sys.exit()
        case args if 'i' in args or '--init' in args or '-u' in args or '--updatekeys' in args:
            updatekeys()
            main()
        case _:
            main()

    # input('Press anykey to exit')
