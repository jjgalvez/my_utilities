#!/usr/bin/env python3

# line this is replacing
# gnome-terminal -e 'sh -c  "sudo pikaur -Syu ; echo Done - Press enter to exit; read" '

import subprocess
import sys


def checkInput(question, answerset, default=False):
    '''
    ask a questoin and check the answer against
    an answer set. return the answer or default
    The default answer is answerset[0] if not explicetly set
    '''
    # a = input(question).lower()
    # if not a:
    #     a = 'ENTER'
    # return a in answers
    default = default if default else answerset[0]
    a = input(question).lower().strip()
    return a if a in answerset else default


def main():
    # update packages 
    Print('Updating package list')
    subprocess.run('sudo apt update', shell=True)

    # upgrade system
    if checkInput('Upgrade ? [Y/n] ', ('y', 'n')) == 'y':
        subprocess.run('sudo apt upgrade')

    # autoclean?
    if checkInput("Clean old package cache? [Y/n] ", ('y', 'n')) == 'y':
        subprocess.run('sudo apt autoclean', shell=True)

    # autoremove?
    if checkInput("Clean old (orphan) softwware packages [y/N] ", ('n','y')) == 'y'
        subprocess.run('sudo apt autoremove', shell=True)

    # # Since flatpak does not update by itself update it at part of mysystem updates
    # if checkInput('Update Flatpak apps? [Y/n]: ', ('y','n')) == 'y':
    #     print('updating Flatpak applications')
    #     subprocess.run('sudo flatpak update -y', shell=True)

    # if checkInput('Remove unused flatpaks - often over removes resources? [y/N]: ',
    #     ('n', 'y')) == 'y':   
    #     print('Removing unused flatpaks')
    #     subprocess.run('sudo flatpak uninstall --unused', shell=True)
    #     # subprocess.run('flatpak update -y', shell=True)

    # reboot case now, midnight, or no
    case = checkInput('Reboot system now or schedual at midnight? [y/N/(m)idnight]: ', ('n', 'y', 'm'))
    if case == 'n':
        pass
    elif case == 'y':
        subprocess.run('sudo reboot now', shell=True)
    elif case == 'm':
        subprocess.run('sudo shutdown -r 23:30', shell=True)
    


if __name__ == '__main__':
    main()

    # exit
    input('Press anykey to exit')
